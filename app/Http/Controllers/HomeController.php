<?php

namespace App\Http\Controllers;

use App\User;
use App\Model\Guru;
use App\Model\Kelas;
use App\Model\Mapel;
use App\Model\Siswa;
use App\Model\Jadwal;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $mapel = Mapel::count();
        $kelas = Kelas::count();
        $siswa = Siswa::count();
        $guru = Guru::count();
        $jadwal = Jadwal::count();
        $user = User::count();
        return view('layout/main', compact('mapel', 'kelas', 'siswa', 'guru', 'jadwal', 'user'));
    }
}