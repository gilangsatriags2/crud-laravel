<?php

namespace App\Http\Controllers\Api;

use App\Model\Guru;
use App\Model\Kelas;
use App\Model\Mapel;
use App\Model\Jadwal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class JadwalController extends Controller
{
    private function
        success(
        $data,
        $statusCode,
        $message = 'success'
    )
    {
        return response()->json([
            'status' => true,
            'message' => $message,
            'data' => $data,
            'status_code' => $statusCode
        ], $statusCode);
    }
    private function
        failedResponse(
        $message,
        $statusCode
    )
    {
        return response()->json([
            'status' => false,
            'message' => $message,
            'data' => null,
            'status_code' => $statusCode
        ], $statusCode);
    }
    public function index()
    {
        $dataRelasi = Jadwal::with('kelas', 'mapel', 'guru')->paginate(5);
        return view('jadwal/jadwal', compact('dataRelasi'));
    }

    public function create()
    {
        $kelas = Kelas::all();
        $guru = Guru::all();
        $mapel = Mapel::all();
        return view('jadwal/create', compact('kelas', 'guru', 'mapel'));
    }

    public function edit($jadwal)
    {
        $kelas = Kelas::all();
        $guru = Guru::all();
        $mapel = Mapel::all();
        $jadwal = Jadwal::with('kelas', 'guru', 'mapel')->findorfail($jadwal);
        return view('jadwal/edit', compact('kelas', 'guru', 'mapel', 'jadwal'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kelas_id' => 'required|integer',
            'mapel_id' => 'required|integer',
            'guru_id' => 'required|integer',
            'hari' => 'required|string:senin,selasa,rabu,kamis,jumat,sabtu',
            'jam_pelajaran' => 'required|string'
        ]);

        if ($validator->fails()) {
            $msg = $validator->errors();

            return $this->failedResponse($msg, 422);
        }

        $jadwal = new Jadwal();
        $jadwal->kelas_id = $request->kelas_id;
        $jadwal->mapel_id = $request->mapel_id;
        $jadwal->guru_id = $request->guru_id;
        $jadwal->hari = $request->hari;
        $jadwal->jam_pelajaran = $request->jam_pelajaran;

        $saveJadwal = $jadwal->save();
        if ($saveJadwal) {
            return redirect()->action('Api\JadwalController@index')->with('toast_success', 'Data berhasil ditambahkan');
        } else {
            return redirect()->action('Api\JadwalController@index')->with('toast_error', 'Data gagal ditambahkan');
        }
    }

    public function show(Jadwal $jadwal)
    {
        //
    }

    public function update(Request $request, Jadwal $jadwal)
    {
        $validator = Validator::make($request->all(), [
            'kelas_id' => 'required|integer',
            'mapel_id' => 'required|integer',
            'guru_id' => 'required|integer',
            'hari' => 'required|string:senin,selasa,rabu,kamis,jumat,sabtu',
            'jam_pelajaran' => 'required|string'
        ]);

        if ($validator->fails()) {
            $msg = $validator->errors();
            return $this->failedResponse($msg, 422);
        }

        $jadwal->kelas_id = $request->kelas_id;
        $jadwal->mapel_id = $request->mapel_id;
        $jadwal->guru_id = $request->guru_id;
        $jadwal->hari = $request->hari;
        $jadwal->jam_pelajaran = $request->jam_pelajaran;
        $saved = $jadwal->save();

        if ($saved) {
            return redirect()->action('Api\JadwalController@index')->with('toast_success', 'Data berhasil diupdate');
        } else {
            return redirect()->action('Api\JadwalController@index')->with('toast_error', 'Data gagal diupdate');
        }
    }

    public function destroy(Jadwal $jadwal)
    {
        $deleteData = $jadwal->delete();

        if ($deleteData) {
            return redirect()->action('Api\JadwalController@index')->with('success', 'Data berhasil dihapus');
        } else {
            return redirect()->action('Api\JadwalController@index')->with('errors', 'Data gagal dihapus');
        }
    }
}
