<?php

namespace App\Http\Controllers\Api;

use App\Model\Kelas;
use App\Model\Siswa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SiswaController extends Controller
{
    private function
        success(
        $data,
        $statusCode,
        $message = 'success'
    )
    {
        return response()->json([
            'status' => true,
            'message' => $message,
            'data' => $data,
            'status_code' => $statusCode
        ], $statusCode);
    }
    private function
        failedResponse(
        $message,
        $statusCode
    )
    {
        return response()->json([
            'status' => false,
            'message' => $message,
            'data' => null,
            'status_code' => $statusCode
        ], $statusCode);
    }
    public function index()
    {
        $data = Siswa::with('kelas')->paginate(5);
        return view('siswa/siswa')->with('data', $data);
    }

    public function create()
    {
        $kelas = Kelas::all();
        return view('siswa/create', compact('kelas'));
    }

    public function edit($siswa)
    {
        $kelas = Kelas::all();
        $siswa = Siswa::with('kelas')->findorfail($siswa);
        return view('siswa/edit', compact('kelas', 'siswa'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nis' => 'required|string',
            'nama' => 'required|string',
            'gender' => 'required|string:laki-laki,perempuan',
            'tempat_lahir' => 'required|string',
            'tgl_lahir' => 'required|date',
            'email' => 'required|string',
            'nama_ortu' => 'required|string',
            'alamat' => 'required|string',
            'phone_number' => 'required|string',
            'kelas_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            $msg = $validator->errors();

            return $this->failedResponse($msg, 422);
        }

        $siswa = new Siswa();
        $siswa->nis = $request->nis;
        $siswa->nama = $request->nama;
        $siswa->gender = $request->gender;
        $siswa->tempat_lahir = $request->tempat_lahir;
        $siswa->tgl_lahir = $request->tgl_lahir;
        $siswa->email = $request->email;
        $siswa->nama_ortu = $request->nama_ortu;
        $siswa->alamat = $request->alamat;
        $siswa->phone_number = $request->phone_number;
        $siswa->kelas_id = $request->kelas_id;

        $saveSiswa = $siswa->save();
        if ($saveSiswa) {
            return redirect()->action('Api\SiswaController@index')->with('toast_success', 'Data berhasil ditambahkan');
        } else {
            return redirect()->action('Api\SiswaController@index')->with('toast_error', 'Data gagal ditambahkan');
        }
    }

    public function show(Siswa $siswa)
    {
        //
    }

    public function update(Request $request, Siswa $siswa)
    {
        $validator = Validator::make($request->all(), [
            'nis' => 'required|string',
            'nama' => 'required|string',
            'gender' => 'required|string:laki-laki,perempuan',
            'tempat_lahir' => 'required|string',
            'tgl_lahir' => 'required|date',
            'email' => 'required|string',
            'nama_ortu' => 'required|string',
            'alamat' => 'required|string',
            'phone_number' => 'required|string',
            'kelas_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            $msg = $validator->errors();

            return $this->failedResponse($msg, 422);
        }

        $siswa->nis = $request->nis;
        $siswa->nama = $request->nama;
        $siswa->gender = $request->gender;
        $siswa->tempat_lahir = $request->tempat_lahir;
        $siswa->tgl_lahir = $request->tgl_lahir;
        $siswa->email = $request->email;
        $siswa->nama_ortu = $request->nama_ortu;
        $siswa->alamat = $request->alamat;
        $siswa->phone_number = $request->phone_number;
        $siswa->kelas_id = $request->kelas_id;

        $saved = $siswa->save();
        if ($saved) {
            return redirect()->action('Api\SiswaController@index')->with('toast_success', 'Data berhasil diupdate');
        } else {
            return redirect()->action('Api\SiswaController@index')->with('toast_error', 'Data gagal diupdate');
        }
    }

    public function destroy(Siswa $siswa)
    {
        $deleteData = $siswa->delete();

        if ($deleteData) {
            return redirect()->action('Api\SiswaController@index')->with('success', 'Data berhasil dihapus');
        } else {
            return redirect()->action('Api\SiswaController@index')->with('errors', 'Data gagal dihapus');
        }
    }
}