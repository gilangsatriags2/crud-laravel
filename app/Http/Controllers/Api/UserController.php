<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    private function
        success(
        $data,
        $statusCode,
        $message = 'success'
    )
    {
        return response()->json([
            'status' => true,
            'message' => $message,
            'data' => $data,
            'status_code' => $statusCode
        ], $statusCode);
    }
    private function
        failedResponse(
        $message,
        $statusCode
    )
    {
        return response()->json([
            'status' => false,
            'message' => $message,
            'data' => null,
            'status_code' => $statusCode
        ], $statusCode);
    }
    public function index()
    {
        $data = User::orderBy('id', 'asc')->paginate(5);
        return view('user/user',compact('data'));
    }

    public function create()
    {
        return view('user/create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|in:admin,guru,dosen',
            'username' => 'required|string',
            'email' => 'required|string',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            $msg = $validator->errors();

            return $this->failedResponse($msg, 422);
        }

        $user = new User();
        $user->type = $request->type;
        $user->username = $request->username;
        $user->password = Hash::make($request->password);
        $user->email = $request->email;

        $saveUser = $user->save();
        if ($saveUser) {
            return redirect()->action('Api\UserController@index')->with('toast_success', 'Data berhasil ditambahkan');
        } else {
            return redirect()->action('Api\UserController@index')->with('toast_error', 'Data gagal ditambahkan');
        }
    }

    public function show(User $user)
    {
        return $this->success($user, 200);
    }

    public function edit($user)
    {
        $cari = User::find($user, ['id', 'type', 'username', 'email', 'password']);
        return view('user/edit', compact('cari'));
    }

    public function update(Request $request, User $user)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|in:admin,guru,dosen',
            'username' => 'required|string',
            'email' => 'required|string',
            'password' => 'nullable|min:6'
        ]);

        if ($validator->fails()) {
            $msg = $validator->errors();
            return $this->failedResponse($msg, 422);
        }

        $user->type = $request->type;
        $user->username = $request->username;
        if ($request->has('password')) {
            $user->password = $request->password ? Hash::make($request->password) : $user->password;
        }
        $user->email = $request->email;
        $saved = $user->save();

        if ($saved) {
            return redirect()->action('Api\UserController@index')->with('toast_success', 'Data berhasil diupdate');
        } else {
            return redirect()->action('Api\UserController@index')->with('toast_error', 'Data gagal diupdate');
        }
    }


    public function destroy(User $user)
    {
        $deleteData = $user->delete();

        if ($deleteData) {
            return redirect()->action('Api\UserController@index')->with('success', 'Data berhasil dihapus');
        } else {
            return redirect()->action('Api\UserController@index')->with('errors', 'Data gagal dihapus');
        }
    }
}