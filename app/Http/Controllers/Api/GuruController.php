<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Model\Guru;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class GuruController extends Controller
{
    private function
        success(
        $data,
        $statusCode,
        $message = 'success'
    )
    {
        return response()->json([
            'status' => true,
            'message' => $message,
            'data' => $data,
            'status_code' => $statusCode
        ], $statusCode);
    }
    private function
        failedResponse(
        $message,
        $statusCode
    )
    {
        return response()->json([
            'status' => false,
            'message' => $message,
            'data' => null,
            'status_code' => $statusCode
        ], $statusCode);
    }
    public function index()
    {
        $data = Guru::with('user')->paginate(5);
        return view('guru/guru', compact('data'));
    }

    public function create()
    {
        $user = User::all();
        return view('guru/create', compact('user'));
    }

    public function edit($guru)
    {
        $user = User::all();
        $guru = Guru::with('user')->findorfail($guru);
        return view('guru/edit', compact('user', 'guru'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|string',
            'nip' => 'required|string',
            'nama' => 'required|string',
            'tempat_lahir' => 'required|string',
            'tgl_lahir' => 'required|date',
            'gender' => 'required|string:laki-laki,perempuan',
            'phone_number' => 'required|string',
            'email' => 'required|string',
            'alamat' => 'required|string',
            'pendidikan' => 'required|string',
        ]);

        if ($validator->fails()) {
            $msg = $validator->errors();

            return $this->failedResponse($msg, 422);
        }

        $guru = new Guru();
        $guru->user_id = $request->user_id;
        $guru->nip = $request->nip;
        $guru->nama = $request->nama;
        $guru->tempat_lahir = $request->tempat_lahir;
        $guru->tgl_lahir = $request->tgl_lahir;
        $guru->gender = $request->gender;
        $guru->phone_number = $request->phone_number;
        $guru->email = $request->email;
        $guru->alamat = $request->alamat;
        $guru->pendidikan = $request->pendidikan;

        $saveGuru = $guru->save();
        if ($saveGuru) {
            return redirect()->action('Api\GuruController@index')->with('toast_success', 'Data berhasil ditambahkan');
        } else {
            return redirect()->action('Api\GuruController@index')->with('toast_error', 'Data gagal ditambahkan');
        }
    }

    public function show(Guru $guru)
    {
        return $this->success($guru, 200);
    }

    public function update(Request $request, Guru $guru)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|string',
            'nip' => 'required|string',
            'nama' => 'required|string',
            'tempat_lahir' => 'required|string',
            'tgl_lahir' => 'required|date',
            'gender' => 'required|string:laki-laki,perempuan',
            'phone_number' => 'required|string',
            'email' => 'required|string',
            'alamat' => 'required|string',
            'pendidikan' => 'required|string',
        ]);

        if ($validator->fails()) {
            $msg = $validator->errors();

            return $this->failedResponse($msg, 422);
        }

        $guru->user_id = $request->user_id;
        $guru->nip = $request->nip;
        $guru->nama = $request->nama;
        $guru->tempat_lahir = $request->tempat_lahir;
        $guru->tgl_lahir = $request->tgl_lahir;
        $guru->gender = $request->gender;
        $guru->phone_number = $request->phone_number;
        $guru->email = $request->email;
        $guru->alamat = $request->alamat;
        $guru->pendidikan = $request->pendidikan;

        $saved = $guru->save();
        if ($saved) {
            return redirect()->action('Api\GuruController@index')->with('toast_success', 'Data berhasil diupdate');
        } else {
            return redirect()->action('Api\GuruController@index')->with('toast_success', 'Data gagal diupdate');
        }
    }

    public function destroy(Guru $guru)
    {
        $deleteData = $guru->delete();

        if ($deleteData) {
            return redirect()->action('Api\GuruController@index')->with('success', 'Data berhasil dihapus');
        } else {
            return redirect()->action('Api\GuruController@index')->with('errors', 'Data gagal dihapus');
        }
    }
}