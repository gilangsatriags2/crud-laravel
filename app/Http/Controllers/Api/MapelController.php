<?php

namespace App\Http\Controllers\Api;

use App\Model\Mapel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class MapelController extends Controller
{
    private function
        success(
        $data,
        $statusCode,
        $message = 'success'
    )
    {
        return response()->json([
            'status' => true,
            'message' => $message,
            'data' => $data,
            'status_code' => $statusCode
        ], $statusCode);
    }
    private function
        failedResponse(
        $message,
        $statusCode
    )
    {
        return response()->json([
            'status' => false,
            'message' => $message,
            'data' => null,
            'status_code' => $statusCode
        ], $statusCode);
    }
    public function index()
    {
        $data = Mapel::orderBy('id', 'asc')->paginate(5);
        return view('mapel/mapel')->with('data', $data);

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kode_mapel' => 'required|string',
            'nama_mapel' => 'required|string',
        ]);

        if ($validator->fails()) {
            $msg = $validator->errors();

            return $this->failedResponse($msg, 422);
        }

        $mapel = new Mapel();
        $mapel->kode_mapel = $request->kode_mapel;
        $mapel->nama_mapel = $request->nama_mapel;

        $saveMapel = $mapel->save();
        if ($saveMapel) {
            return redirect()->action('Api\MapelController@index')->with('toast_success', 'Data berhasil ditambahkan');
        } else {
            return redirect()->action('Api\MapelController@index')->with('toast_error', 'Data gagal ditambahkan');
        }
    }

    public function create()
    {
        return view('mapel/create');
    }

    public function show(Mapel $mapel)
    {
        //
    }

    public function edit($mapel)
    {
        $cari = mapel::find($mapel, ['id', 'kode_mapel', 'nama_mapel']);
        return view('mapel/edit', compact('cari'));
    }

    public function update(Request $request, Mapel $mapel)
    {
        $validator = Validator::make($request->all(), [
            'kode_mapel' => 'required|string',
            'nama_mapel' => 'required|string'
        ]);

        if ($validator->fails()) {
            $msg = $validator->errors();
            return $this->failedResponse($msg, 422);
        }

        $mapel->kode_mapel = $request->kode_mapel;
        $mapel->nama_mapel = $request->nama_mapel;
        $saved = $mapel->save();

        if ($saved) {
            return redirect()->action('Api\MapelController@index')->with('toast_success', 'Data berhasil diupdate');
        } else {
            return redirect()->action('Api\MapelController@index')->with('toast_error', 'Data gagal diupdate');
        }
    }

    public function destroy(Mapel $mapel)
    {
        $deleteData = $mapel->delete();

        if ($deleteData) {
            return redirect()->action('Api\MapelController@index')->with('success', 'Data berhasil dihapus');
        } else {
            return redirect()->action('Api\MapelController@index')->with('errors', 'Data gagal dihapus');
        }
    }
}
