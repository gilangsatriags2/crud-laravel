<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Kelas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class KelasController extends Controller
{
  private function
    success(
    $data,
    $statusCode,
    $message = 'success'
  )
  {
    return response()->json([
      'status' => true,
      'message' => $message,
      'data' => $data,
      'status_code' => $statusCode
    ], $statusCode);
  }
  private function
    failedResponse(
    $message,
    $statusCode
  )
  {
    return response()->json([
      'status' => false,
      'message' => $message,
      'data' => null,
      'status_code' => $statusCode
    ], $statusCode);
  }
  public function index()
  {
    $data = Kelas::orderBy('id', 'asc')->paginate(5);
    return view('kelas/kelas', compact('data'));
  }

  public function store(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'kode_kelas' => 'required|string',
      'nama_kelas' => 'required|string',
    ]);

    if ($validator->fails()) {
      $msg = $validator->errors();

      return $this->failedResponse($msg, 422);
    }
    $kelas = new Kelas();
    $kelas->kode_kelas = $request->kode_kelas;
    $kelas->nama_kelas = $request->nama_kelas;

    $saveKelas = $kelas->save();
    if ($saveKelas) {
      return redirect()->action('Api\KelasController@index')->with('toast_success', 'Data berhasil ditambahkan');
    } else {
      return redirect()->action('Api\KelasController@index')->with('toast_error', 'Data gagal ditambahkan');
    }
  }


  public function show(Kelas $kelas)
  {
    //
  }



  public function create()
  {
    return view('kelas/create');
  }


  public function edit($kelas)
  {
    $cari = kelas::find($kelas, ['id', 'kode_kelas', 'nama_kelas']);
    return view('kelas/edit', compact('cari'));
  }

  public function update(Request $request, Kelas $kelas)
  {
    $validator = Validator::make($request->all(), [
      'kode_kelas' => 'required|string',
      'nama_kelas' => 'required|string'
    ]);

    if ($validator->fails()) {
      $msg = $validator->errors();
      return $this->failedResponse($msg, 422);
    }

    $kelas->kode_kelas = $request->kode_kelas;
    $kelas->nama_kelas = $request->nama_kelas;
    $saved = $kelas->save();

    if ($saved) {
      return redirect()->action('Api\KelasController@index')->with('toast_success', 'Data berhasil diupdate');
    } else {
      return redirect()->action('Api\KelasController@index')->with('toast_error', 'Data gagal diupdate');
    }
  }

  public function destroy(Kelas $kelas)
  {
    $deleteData = $kelas->delete();

    if ($deleteData) {
      return redirect()->action('Api\KelasController@index')->with('success', 'Data berhasil dihapus');
    } else {
      return redirect()->action('Api\KelasController@index')->with('errors', 'Data gagal dihapus');
    }
  }
}