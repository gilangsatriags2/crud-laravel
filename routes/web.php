<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin', 'AdminController@index');
Route::get('/', 'HomeController@index');
Route::get('/user', 'Api\UserController@index');
Route::get('/guru', 'Api\GuruController@index');
Route::get('/jadwal', 'Api\JadwalController@index');
Route::get('/mapel', 'Api\MapelController@index');
Route::get('/kelas', 'Api\KelasController@index');
Route::get('/siswa', 'Api\SiswaController@index');
Route::get('/user', 'Api\UserController@index');

Route::get('/mapel/form_post', 'Api\MapelController@create');
Route::post('/mapel/form_post/data', 'Api\MapelController@store');
Route::delete('/mapel/delete/{mapel}', 'Api\MapelController@destroy');
Route::get('/mapel/edit_mapel/{mapel}', 'Api\MapelController@edit');
Route::put('/mapel/edit_mapel/data/{mapel}', 'Api\MapelController@update');


Route::get('/kelas/form_post', 'Api\KelasController@create');
Route::post('/kelas/form_post/data', 'Api\KelasController@store');
Route::delete('/kelas/delete/{kelas}', 'Api\KelasController@destroy');
Route::get('/kelas/edit_kelas/{kelas}', 'Api\KelasController@edit');
Route::put('/kelas/edit_kelas/data/{kelas}', 'Api\KelasController@update');

Route::get('/guru/form_post', 'Api\GuruController@create');
Route::post('/guru/form_post/data', 'Api\GuruController@store');

Route::get('/jadwal/form_post', 'Api\JadwalController@create');
Route::post('/jadwal/form_post/data', 'Api\JadwalController@store');
Route::get('/jadwal/edit_jadwal/{jadwal}', 'Api\JadwalController@edit');
Route::put('/jadwal/edit_jadwal/data/{jadwal}', 'Api\JadwalController@update');
Route::delete('/jadwal/delete/{jadwal}', 'Api\JadwalController@destroy');

Route::get('/siswa/form_post', 'Api\SiswaController@create');
Route::post('/siswa/form_post/data', 'Api\SiswaController@store');
Route::delete('/siswa/delete/{siswa}', 'Api\SiswaController@destroy');
Route::get('/siswa/edit_siswa/{siswa}', 'Api\SiswaController@edit');
Route::put('/siswa/edit_siswa/data/{siswa}', 'Api\SiswaController@update');

Route::get('/guru/form_post', 'Api\GuruController@create');
Route::post('/guru/form_post/data', 'Api\GuruController@store');
Route::delete('/guru/delete/{guru}', 'Api\GuruController@destroy');
Route::get('/guru/edit_guru/{guru}', 'Api\GuruController@edit');
Route::put('/guru/edit_guru/data/{guru}', 'Api\GuruController@update');

Route::get('/user/form_post', 'Api\UserController@create');
Route::post('/user/form_post/data', 'Api\UserController@store');
Route::delete('/user/delete/{user}', 'Api\UserController@destroy');
Route::get('/user/edit_user/{user}', 'Api\UserController@edit');
Route::put('/user/edit_user/data/{user}', 'Api\UserController@update');