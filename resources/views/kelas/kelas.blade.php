@include('layout/header')
@include('layout/navbar')
@include('layout/sidebar')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                    <h1>Data Kelas</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{action('HomeController@index')}}">Home</a></li>
                        <li class="breadcrumb-item active">Data Kelas</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <button class="btn btn-success btn-sm"><a href="/kelas/form_post" style="text-decoration: none;"
                class="text-light"><i class="nav-icon fas fa-light fa-plus"></i> Tambah
                Kelas</a></button>
        <div><br>
        </div>
        <!-- Default box -->
        <div class="card">
            <table class="table table-bordered text-center table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kode Kelas</th>
                        <th>Nama Kelas</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $x = $data->firstItem() ?>
                    @foreach($data as $value)
                        <tr>
                            <td>
                                {{ $x }}
                            </td>
                            <td>
                                {{ $value->kode_kelas }}
                            </td>
                            <td>
                                {{ $value->nama_kelas }}
                            </td>
                            <td>
                                <div class="row justify-content-center">
                                    <a href="{{ action('Api\KelasController@edit',['kelas'=>$value->id]) }}"
                                        class="btn btn-warning btn-sm mx-2"><i class="fa-solid fa-pen"></i></a>
                                    <form
                                        action="{{ action('Api\KelasController@destroy',['kelas'=>$value->id]) }}"
                                        method="post" class="mx-2" onsubmit="return confirm('Yakin hapus data ?')">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-danger btn-sm" type="submit"><i
                                                class="fa-solid fa-trash"></i></button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        <?php
          $x += 1;
        ?>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="pagination pagination-sm m-0 float-right">
            {{ $data->links() }}
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@include('layout/footer')
