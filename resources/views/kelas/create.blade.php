@include('layout/header')
@include('layout/navbar')
@include('layout/sidebar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header text-primary text-center"><h5>Tambah Data Kelas</h5></div>
                        <div class="card-body">
                            <form method="POST" action="/kelas/form_post/data">
                                @csrf
                                <div class="form-group">
                                    <label>Kode Kelas</label>
                                    <input type="text" class="form-control" name="kode_kelas" required>
                                </div>
                                <div class=" form-group">
                                    <label>Nama Kelas</label>
                                    <input type="text" class="form-control" name="nama_kelas" required>
                                </div>
                                <div>
                                    <button class="btn btn-primary btn-sm">Simpan</button>
                                    <a class="btn btn-danger btn-sm" href="{{action('Api\KelasController@index')}}">Batal</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@include('layout/footer')
