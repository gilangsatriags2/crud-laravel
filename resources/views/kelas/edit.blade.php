@include('layout/header')
@include('layout/navbar')
@include('layout/sidebar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header text-center text-primary"><h5>Edit Kelas</h5></div>
                        <div class="card-body">
                            <form method="POST"
                                action="{{ action('Api\KelasController@update',['kelas'=>$cari->id]) }}">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <div class="form-group">
                                    <label>Kode Kelas</label>
                                    <input type="text" class="form-control" name="kode_kelas"
                                        value="{{ $cari->kode_kelas }}" required>
                                </div>
                                <div class=" form-group">
                                    <label>Nama Kelas</label>
                                    <input type="text" class="form-control" name="nama_kelas"
                                        value="{{ $cari->nama_kelas }}" required>
                                </div>
                                <div>
                                    <button class="btn btn-primary btn-sm">Simpan</button>
                                    <a class="btn btn-danger btn-sm" href="{{action('Api\MapelController@index')}}">Batal</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@include('layout/footer')
