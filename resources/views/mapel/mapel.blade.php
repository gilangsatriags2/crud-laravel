@include('layout/header')
@include('layout/navbar')
@include('layout/sidebar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data Mapel</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{action('HomeController@index')}}">Home</a></li>
                        <li class="breadcrumb-item active">Data Mapel</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <button class="btn btn-success btn-sm"><a href="/mapel/form_post" style="text-decoration: none;"
                class="text-light"><i class="nav-icon fas fa-light fa-plus"></i> Tambah
                Mapel</a></button>

        <div><br></div>
        <div class="card">

            <!-- Default box -->
            <table class="table table-bordered text-center table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kode Mapel</th>
                        <th>Nama Mapel</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $x = $data->firstItem() ?>
                    @foreach($data as $value)
                        <tr>
                            <td>
                                <?php echo $x; ?>
                            </td>
                            <td>
                                {{ $value->kode_mapel }}
                            </td>
                            <td>
                                {{ $value->nama_mapel }}
                            </td>
                            <td>
                                <div class="row justify-content-center">
                                    <a href="{{ action('Api\MapelController@edit',['mapel'=>$value->id]) }}"
                                        class="btn btn-warning btn-sm mx-2"><i class="fa-solid fa-pen"></i></a>
                                    <form
                                        action="{{ action('Api\MapelController@destroy',['mapel'=>$value->id]) }}"
                                        method="post" class="mx-2" onsubmit="return confirm('Yakin hapus data ?')">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-danger btn-sm" type="submit"><i
                                                class="fa-solid fa-trash"></i></button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        <?php $x += 1; ?>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="pagination pagination-sm m-0 float-right">
          {{ $data->links() }}
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@include('layout/footer')
