@include('layout/header')
@include('layout/navbar')
@include('layout/sidebar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header text-center text-primary"><h5>Tambah User</h5></div>
                        <div class="card-body">
                            @if ($errors->any())
                            <div class="pt-3">
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $item)
                                        <li>{{$item}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            @endif
                            <form method="POST" action="/user/form_post/data">
                                @csrf
                                <div class="form-group">
                                    <label>Tipe</label>
                                    <select name="type" class="form-select form-control" aria-label="Default select example"
                                        required>
                                        <option hidden selected>Pilih</option>
                                        <option value="admin">Admin</option>
                                        <option value="guru">Guru</option>
                                        <option value="dosen">Dosen</option>
                                    </select>
                                </div>
                                <div class=" form-group">
                                    <label>Username</label>
                                    <input type="text" class="form-control" name="username" required>
                                </div>
                                <div class=" form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email" required>
                                </div>
                                <div class=" form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password" required>
                                </div>
                                <div>
                                    <button class="btn btn-primary btn-sm">Simpan</button>
                                    <a class="btn btn-danger btn-sm" href="{{action('Api\UserController@index')}}">Batal</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@include('layout/footer')
