@include('layout/header')
@include('layout/navbar')
@include('layout/sidebar')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                    <h1>Data Jadwal</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{action('HomeController@index')}}">Home</a></li>
                        <li class="breadcrumb-item active">Data Jadwal</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <button class="btn btn-success btn-sm"><a href="/jadwal/form_post" style="text-decoration: none;"
                class="text-light"><i class="nav-icon fas fa-light fa-plus"></i> Tambah
                Jadwal</a></button>
        <div><br>
        </div>
        <div class="card">

            <!-- Default box -->
            <table class="table table-bordered text-center table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kelas</th>
                        <th>Mapel</th>
                        <th>Guru</th>
                        <th>Hari</th>
                        <th>Jam Pelajaran</th>
                        <th>Aksi</th>
                    </tr>
                </thead>

                <tbody>
                    <?php $i = $dataRelasi->firstItem() ?>
                    @foreach($dataRelasi as $x)
                        <tr>
                            <td>
                                {{ $i }}
                            </td>
                            <td>
                                {{ $x->kelas->nama_kelas }}
                            </td>
                            <td>
                                {{ $x->mapel->nama_mapel }}
                            </td>
                            <td>
                                {{ $x->guru->nama }}
                            </td>
                            <td>
                                {{ $x->hari }}
                            </td>
                            <td>
                                {{ $x->jam_pelajaran }}
                            </td>
                            <td>
                                <div class="row justify-content-center">
                                    <a href="{{ action('Api\JadwalController@edit',['jadwal'=>$x->id]) }}"
                                        class="btn btn-warning btn-sm mx-2"><i class="fa-solid fa-pen"></i></a>
                                    <form
                                        action="{{ action('Api\JadwalController@destroy',['jadwal'=>$x->id]) }}"
                                        method="post" class="mx-2"  onsubmit="return confirm('Yakin hapus data ?')">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-danger btn-sm" type="submit"><i
                                                class="fa-solid fa-trash"></i></button>
                                    </form>
                                </div>
                            </td>
                            <?php $i += 1; ?>
                    @endforeach
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="pagination pagination-sm m-0 float-right">
            {{ $dataRelasi->links() }}
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@include('layout/footer')
