@include('layout/header')
@include('layout/navbar')
@include('layout/sidebar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header text-center text-primary"><h5>Edit Jadwal</h5></div>
                        <div class="card-body">
                            <form method="POST"
                                action="{{ action('Api\JadwalController@update',['jadwal'=>$jadwal->id]) }}">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <div class="form-group">
                                    <label>ID Kelas</label>
                                    <select name="kelas_id" class="form-select form-control" aria-label="Default select example"
                                        required>
                                        <option disabled>Pilih Kelas</option>
                                        <option value="{{ $jadwal->kelas_id }}" hidden>
                                            {{ $jadwal->kelas->nama_kelas }}</option>
                                        @foreach($kelas as $x)
                                            <option value="{{$x->id}}">{{ $x->nama_kelas }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class=" form-group">
                                    <label>ID Mapel</label>
                                    <select name="mapel_id" class="form-select form-control" aria-label="Default select example"
                                        required>
                                        <option disabled>Pilih</option>
                                        <option value="{{ $jadwal->mapel_id }}" hidden>
                                            {{ $jadwal->mapel->nama_mapel }}</option>
                                        @foreach($mapel as $x)
                                            <option value="{{$x->id}}">{{ $x->nama_mapel }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class=" form-group">
                                    <label>ID Guru</label>
                                    <select name="guru_id" class="form-select form-control" aria-label="Default select example"
                                        required>
                                        <option disabled>Pilih</option>
                                        <option value="{{ $jadwal->guru_id }}" hidden>{{ $jadwal->guru->nama }}
                                        </option>
                                        @foreach($guru as $x)
                                            <option value="{{$x->id}}">{{ $x->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class=" form-group">
                                    <label>Hari</label>
                                    <select name="hari" class="form-select form-control" aria-label="Default select example"
                                        required>
                                        <option selected>Pilih Hari</option>
                                        <option value="{{ $jadwal->hari }}" selected hidden>{{ $jadwal->hari }}
                                        </option>
                                        <option value="senin">Senin</option>
                                        <option value="selasa">Selasa</option>
                                        <option value="rabu">rabu</option>
                                        <option value="kamis">Kamis</option>
                                        <option value="jumat">Jumat</option>
                                        <option value="sabtu">Sabtu</option>
                                    </select>
                                </div>
                                <div class=" form-group">
                                    <label>Jam Pelajaran</label>
                                    <input type="text" class="form-control" name="jam_pelajaran"
                                        value="{{ $jadwal->jam_pelajaran }}" required>
                                </div>
                                <div>
                                    <button class="btn btn-primary btn-sm">Simpan</button>
                                    <a class="btn btn-danger btn-sm" href="{{action('Api\JadwalController@index')}}">Batal</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@include('layout/footer')
