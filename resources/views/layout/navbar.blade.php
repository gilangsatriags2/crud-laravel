<nav class="main-header navbar navbar-expand navbar-white navbar-light">
  <!-- Left navbar links -->
  <ul class="navbar-nav">
    <li class="nav-item mt-1">
      <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
    </li>
    <h3 class="text-primary mt-2">Sistem Informasi Sekolah</h3>
  </ul>
</nav>
<!-- /.navbar -->