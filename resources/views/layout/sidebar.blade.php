<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">

<a href="{{action('HomeController@index')}}" class="brand-link">
  <img src="../../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
    style="opacity: .8">
<span class="brand-text font-weight-light">SEKOLAHAN</span>
</a>

<!-- Sidebar -->
<div class="sidebar">
  <!-- Sidebar user (optional) -->
<div class="user-panel mt-0 pb-3 mb-3 d-flex">
</div>

<!-- SidebarSearch Form -->
<div class="form-inline">
  <div class="input-group" data-widget="sidebar-search">
  <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
  <div class="input-group-append">
    <button class="btn btn-sidebar">
      <i class="fas fa-search fa-fw"></i>
    </button>
  </div>
</div>
</div>

<!-- Sidebar Menu -->
<nav class="mt-2">
  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <li class="nav-item">
      <a href="{{action('HomeController@index')}}" class="nav-link">
        <i class="nav-icon fa-solid fa-gauge"></i>
        <p>
          Dashboard
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{action('Api\MapelController@index')}}" class="nav-link">
        <i class="nav-icon fas fa-light fa-book"></i>
        <p>
          Mapel
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{action('Api\KelasController@index')}}" class="nav-link">
        <i class="nav-icon fa-sharp fa-solid fa-people-roof"></i>
        <p>
          Kelas
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{action('Api\SiswaController@index')}}" class="nav-link">
        <i class="nav-icon fas fa-light fa-user"></i>
        <p>
          Siswa
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{action('Api\GuruController@index')}}" class="nav-link">
        <i class="nav-icon fas fa-light fa-person-chalkboard"></i>
        <p>
          Guru
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{action('Api\JadwalController@index')}}" class="nav-link">
        <i class=" nav-icon fas fa-light fa-clipboard-list"></i>
        <p>
          Jadwal
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{action('Api\UserController@index')}}" class="nav-link">
        <i class="nav-icon fa-solid fa-users"></i>
        <p>
          User
        </p>
      </a>
    </li>

  </ul>
</nav>
<!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
</aside>