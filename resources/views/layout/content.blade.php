<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">
      
      <!-- Jumbotron -->
      <!-- <img src="" class="float-end me-5" width="400px"> -->
      <div class="bg-light ms-2 mb-5 rounded">
        <div class="container-fluid ms-5">
          <h1 class="fw-bold datang">Selamat Datang Admin</h1>
          <h6>Membantu Meningkatkan Efektifitas dan Efisiensi Manajemen Sekolah
            seperti Informasi Guru dan Siswa</h6>
          </div>
        </div>
        <!-- Akhir jumbotron -->
        
        <div class = "content-fluid ms-3">
        <div class="row">
            <div class="col-lg-3 col-6">

                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{$mapel}}</h3>
                        <p>Mapel</p>
                    </div>
                    <div class="icon">
                      <i class="nav-icon fas fa-light fa-book"></i>
                    </div>
                    <a href="{{action('Api\MapelController@index')}}" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-6">

                <div class="small-box bg-success">
                    <div class="inner">
                        <h3>{{$kelas}}</h3>
                        <p>Kelas</p>
                    </div>
                    <div class="icon">
                      <i class="nav-icon fa-sharp fa-solid fa-people-roof"></i>
                    </div>
                    <a href="{{action('Api\KelasController@index')}}" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-6">

                <div class="small-box bg-warning">
                    <div class="inner">
                        <h3>{{$siswa}}</h3>
                        <p>Siswa</p>
                    </div>
                    <div class="icon">
                      <i class="nav-icon fas fa-light fa-user"></i>
                    </div>
                    <a href="{{action('Api\SiswaController@index')}}" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-6">

                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3>{{$guru}}</h3>
                        <p>Guru</p>
                    </div>
                    <div class="icon">
                      <i class="nav-icon fas fa-light fa-person-chalkboard"></i>
                    </div>
                    <a href="{{action('Api\GuruController@index')}}" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-6">

                <div class="small-box bg-warning">
                    <div class="inner">
                        <h3>{{$jadwal}}</h3>
                        <p>Jadwal</p>
                    </div>
                    <div class="icon">
                      <i class=" nav-icon fas fa-light fa-clipboard-list"></i>
                    </div>
                    <a href="{{action('Api\JadwalController@index')}}" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-6">

                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{$user}}</h3>
                        <p>User</p>
                    </div>
                    <div class="icon">
                      <i class="nav-icon fa-solid fa-users"></i>
                    </div>
                    <a href="{{action('Api\UserController@index')}}" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
          </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
