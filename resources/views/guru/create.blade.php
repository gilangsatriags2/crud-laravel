@include('layout/header')
@include('layout/navbar')
@include('layout/sidebar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header text-center text-primary"><h5>Tambah Data Guru</h5></div>
                        <div class="card-body">
                            <form method="POST" action="/guru/form_post/data">
                                @csrf
                                <div class="form-group">
                                    <label>ID</label>
                                    <select name="user_id" class="form-select form-control" aria-label="Default select example"
                                        required>
                                        <option hidden selected value="">Pilih</option>
                                        @foreach($user as $x)
                                            <option value="{{ $x->id }}">{{$x->id}}. {{ $x->type }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class=" form-group">
                                    <label>NIP</label>
                                    <input type="text" class="form-control" name="nip" required>
                                </div>
                                <div class=" form-group">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" name="nama" required>
                                </div>
                                <div class=" form-group">
                                    <label>Tempat Lahir</label>
                                    <input type="text" class="form-control" name="tempat_lahir" required>
                                </div>
                                <div class=" form-group">
                                    <label>Tanggal Lahir</label>
                                    <input type="date" class="form-control" name="tgl_lahir" required>
                                </div>
                                <div class=" form-group">
                                    <label>Jenis Kelamin</label>
                                    <select name="gender" class="form-select form-control" aria-label="Default select example"
                                        required>
                                        <option hidden selected>Pilih</option>
                                        <option value="laki-laki">Laki - Laki</option>
                                        <option value="perempuan">Perempuan</option>
                                    </select>
                                </div>
                                <div class=" form-group">
                                    <label>Nomor Telepon</label>
                                    <input type="text" class="form-control" name="phone_number" required>
                                </div>
                                <div class=" form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email" required>
                                </div>
                                <div class=" form-group">
                                    <label>Alamat</label>
                                    <textarea name="alamat" class="form-control" required></textarea>
                                </div>
                                <div class=" form-group">
                                    <label>Pendidikan</label>
                                    <input type="text" class="form-control" name="pendidikan" required>
                                </div>
                                <div>
                                    <button class="btn btn-primary btn-sm">Simpan</button>
                                    <a class="btn btn-danger btn-sm" href="{{action('Api\GuruController@index')}}">Batal</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@include('layout/footer')
