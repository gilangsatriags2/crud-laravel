@include('layout/header')
@include('layout/navbar')
@include('layout/sidebar')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data Guru</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{action('HomeController@index')}}">Home</a></li>
                        <li class="breadcrumb-item active">Data Guru</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <button class="btn btn-success btn-sm"><a href="/guru/form_post" style="text-decoration: none;"
                class="text-light"><i class="nav-icon fas fa-light fa-plus"></i> Tambah
                Guru</a></button>
        <div><br></div>
        <div class="card">

            <table class="table table-bordered text-center table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Id</th>
                        <th>NIP</th>
                        <th>Nama</th>
                        <th>Tempat Lahir</th>
                        <th>Tanggal Lahir</th>
                        <th>Gender</th>
                        <th>No Telepon</th>
                        <th>Email</th>
                        <th>Alamat</th>
                        <th>Pendidikan</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = $data->firstItem() ?>
                    @foreach($data as $value)
                        <tr>
                            <td>
                                {{ $i }}
                            </td>
                            <td>
                                {{ $value['user_id'] }}
                            </td>
                            <td>
                                {{ $value['nip'] }}
                            </td>
                            <td>
                                {{ $value['nama'] }}
                            </td>
                            <td>
                                {{ $value['tempat_lahir'] }}
                            </td>
                            <td>
                                {{ date('d/m/y', strtotime($value['tgl_lahir'])) }}
                            </td>
                            <td>
                                {{ $value['gender'] }}
                            </td>
                            <td>
                                {{ $value['phone_number'] }}
                            </td>
                            <td>
                                {{ $value['email'] }}
                            </td>
                            <td>
                                {{ $value['alamat'] }}
                            </td>
                            <td>
                                {{ $value['pendidikan'] }}
                            </td>
                            <td>
                                <div class="row justify-content-center">
                                    <a href="{{ action('Api\GuruController@edit',['guru'=>$value->id]) }}"
                                        class="btn btn-warning btn-sm mx-1"><i class="fa-solid fa-pen"></i></a>
                                    <form
                                        action="{{ action('Api\GuruController@destroy',['guru'=>$value->id]) }}"
                                        method="post" onsubmit="return confirm('Yakin hapus data ?')">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-danger btn-sm" type="submit"><i
                                                class="fa-solid fa-trash"></i></button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        <?php $i+=1; ?>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="pagination pagination-sm m-0 float-right">
            {{ $data->links() }}
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@include('layout/footer')
